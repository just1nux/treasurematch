﻿using System;
using System.Collections.Generic;

namespace Model
{
	public class MatchGame
	{
		private List<Model.Chest> openChests;

		public MatchGame ()
		{
			openChests = new List<Model.Chest>();
		}

		public List<Model.Chest> GetOpenChests(){
			return openChests;
		}
	}
}

public class Sorter : IComparer<Model.Chest>
{
	public int Compare(Model.Chest c1, Model.Chest c2)
	{
		return c2.pick.pickObject.CompareTo(c1.pick.pickObject);
	}
}


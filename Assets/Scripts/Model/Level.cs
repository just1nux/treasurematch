﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
	public class Level
	{
		public int numChests; 
		private Dictionary<int, Model.Chest> chests;
		public MatchGame matchGame {get; set;}

		public Level (int numChests = 10)
		{
			matchGame = new MatchGame ();
			this.numChests = numChests; //default to 10 chests
			addChests ();
			addPicks ();
			chests = shuffleChests(chests);
		}

		public Chest GetChest(int index){
			return chests[index];
		}

		private void addChests(){
			chests = new Dictionary<int, Model.Chest>();

			for (int i = 0; i < numChests; i++) {
				chests [i] = new Chest ();
				chests [i].id = i;
			}

		}

		private Dictionary<int, int> addPicks(){
			Dictionary<int, int> picks = new Dictionary<int, int> ();
			int j=0;

			for (int i = 0; i < this.numChests-1; i++) {
				picks.Add(i, j);
				chests [i].pick = new Pick (j);
				if (i % 2 == 0)
					j++;
			}
			picks.Add(this.numChests-1, 0);
			chests [this.numChests-1].pick = new Pick (0);

			return picks;
		}

		private Dictionary<int, Model.Chest> shuffleChests(Dictionary<int, Model.Chest> chests){
			Dictionary<int, Model.Chest> shuffleChests = new Dictionary<int, Model.Chest> ();
			int i = 0;

			System.Random r = new System.Random ();

			while (chests.Count > 0) {
				int t = r.Next (0, chests.Count);
				shuffleChests.Add(i,chests.ElementAt(t).Value);
				chests.Remove (chests.ElementAt(t).Key);
				i++;
			}

			return shuffleChests;
		}
	}
}


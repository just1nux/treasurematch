﻿using System;

namespace Model
{
	public class Chest
	{
		public int id { get; set;}
		public bool isOpen { get; set;}
		public Pick pick { get; set;}
		public bool isVisible { get; set;}
		public Chest ()
		{
			isOpen = false;
			isVisible = true;
		}
	}
}


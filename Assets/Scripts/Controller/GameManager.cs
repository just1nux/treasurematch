﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	//GameManager can be retreived using
	//GameObject.FindWithTag("GameController").GetComponent<GameManager> ();
	//Any managers below can then be accessed.

	public GameObject ChestManagerGameObject;
	public GameObject DataManagerGameObject;

	public ChestManager ChestManager {
		get { return ChestManagerGameObject.GetComponent<ChestManager> (); }
	}

	public DataManager DataManager {
		get { return DataManagerGameObject.GetComponent<DataManager> (); }
	}

}

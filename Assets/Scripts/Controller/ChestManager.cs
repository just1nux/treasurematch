﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Model;

public class ChestManager : MonoBehaviour {
	private GameObject canvas;
	private GameObject panel;
	private GameObject grid;

	private Dictionary<int, GameObject> chests;

	public GameObject chestPrefab;
	public GameObject canvasPrefab;
	public Sprite pickMaskSprite;

	public Sprite[] picks;

	private Level level;

	void Start () {

		DataManager dataManager = GameObject.FindWithTag ("GameController").GetComponent<GameManager> ().DataManager;
		level = dataManager.level;

		chests = new Dictionary<int, GameObject> ();
		AddCanvas ();
		AddPanel ();
		AddGrid ();
		AddChests ();
	}

	private void AddCanvas(){
		//Add a Canvas
		canvas = (GameObject)Instantiate (canvasPrefab);
		Canvas canvasComponent = canvas.GetComponent<Canvas> ();
		canvasComponent.worldCamera = Camera.main;
		canvas.name = "ChestCanvas";

	}

	private void AddPanel() {
		panel = new GameObject ("ChestPanel");
		panel.transform.SetParent (canvas.transform);
		panel.transform.Translate (canvas.transform.position);
		panel.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

		//Set content size fitter
		ContentSizeFitter csf = panel.AddComponent<ContentSizeFitter> ();
		csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
	}

	private void AddGrid(){
		//Add a Grid
		grid = new GameObject ("ChestGrid");
		grid.transform.SetParent (panel.transform);
		grid.transform.Translate (panel.transform.position);
		grid.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

		GridLayoutGroup glg = grid.AddComponent<GridLayoutGroup> ();

		glg.padding = new RectOffset (0,0,0,0);
		glg.spacing = new Vector2 (0, 0);
		glg.cellSize = new Vector2 (100, 100);
		glg.childAlignment = TextAnchor.MiddleCenter;
		glg.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
		glg.constraintCount = 5;

		//Set content size fitter
		ContentSizeFitter csf = grid.AddComponent<ContentSizeFitter> ();
		csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
	}

	private void AddChests(){
		for (int y = 0; y < level.numChests; y++) {
			GameObject chest = (GameObject)Instantiate (chestPrefab);
			chest.transform.SetParent (grid.transform);
			chest.name = "Chest" + y;
			chest.GetComponent<Chest> ().data = level.GetChest (y);
			chests.Add (y, chest);
			AddPick (y);
		}
	}

	private void AddPick(int chestId){
		GameObject chest = GetChestById (chestId);

		GameObject pickMask = new GameObject ("PickMask" + chestId);
		pickMask.transform.SetParent (chest.transform);
		pickMask.transform.localScale = Vector3.one;
		pickMask.transform.localPosition = new Vector3 (0f,6.5f,0f);

		Image pmImage = pickMask.AddComponent<Image> ();
		pmImage.sprite = pickMaskSprite;

		Mask mask = pickMask.AddComponent<Mask> ();
		mask.showMaskGraphic = false;

		GameObject pick = new GameObject ("Pick" + chestId);
		pick.transform.SetParent (pickMask.transform);
		pick.transform.localScale = Vector3.one;
		pick.transform.localPosition = new Vector3 (-10f,-40f,0f);

		Image pickImage = pick.AddComponent<Image> ();
		pickImage.sprite = picks[level.GetChest(chestId).pick.pickObject];
		pickImage.SetNativeSize ();

		chest.GetComponent<Chest> ().pick = pick;

	}

	public GameObject GetChestById(int id){
		GameObject chest;
		chests.TryGetValue (id, out chest);
		return chest;
	}
}


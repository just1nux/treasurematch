﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelButtons : MonoBehaviour {

	public int starCount;
	public bool lockState;

	// Use this for initialization
	void Start () {

		GameObject star1 = (GameObject) this.transform.Find ("Star1").gameObject;
		GameObject star2 = (GameObject) this.transform.Find ("Star2").gameObject;
		GameObject star3 = (GameObject) this.transform.Find ("Star3").gameObject;

		if (starCount >= 1)
			star1.SetActive(true);
		else
			star1.SetActive(false);
		
		if (starCount >= 2)
			star2.SetActive(true);
		else
			star2.SetActive(false);

		if (starCount >= 3)
			star3.SetActive(true);
		else
			star3.SetActive(false);

		//GameObject number = (GameObject) this.transform.Find ("Text").gameObject;
		//number.SetActive(false);

		GameObject lockObject = (GameObject) this.transform.Find ("Lock").gameObject;
		lockObject.SetActive(lockState);

	}

	//public void openLevel(){
		//GameObject button = EventSystem.current.currentSelectedGameObject;
		//LevelManager.Save ();
		//SceneManager.LoadScene ("LevelScreen");
	//	Debug.Log("Load Scene");
	//}


	// Update is called once per frame
	void Update () {

	}
}

﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Model;

public class Chest : MonoBehaviour {

	[Header("Chest Sprites")]
	public Sprite closedChest;
	public Sprite openChest;

	public Model.Chest data { get; set;}
	public GameObject pick { get; set;}

	private float openSpeed = 3.0f;
	private float closeSpeed = 4.0f;

	private Level level;

	// Use this for initialization
	void Start () {
		GameManager gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager> ();
		level = gameManager.DataManager.level;

		gameObject.transform.localScale = new Vector3(1, 1, 1);

		//Programatically add a listener for the onClick event
		Button button = this.gameObject.AddComponent<Button>();
		button.onClick.AddListener (()=> onChestClick(button));
		ColorBlock cb = button.colors;
		cb.pressedColor = new Color(1,1,1,1);
		button.colors = cb;
	}

	void Update () {

		if (data.isOpen) { 
			swapSprite (openChest);
			if (pick.transform.localPosition.y < 20) {
				pick.transform.Translate (Vector3.up * openSpeed * Time.deltaTime); 
			}
		} else {
			if (pick.transform.localPosition.y > -40) {
				pick.transform.Translate (Vector3.down * closeSpeed * Time.deltaTime); 
			} else {
				swapSprite (closedChest);

			}
		}

		if (data.isVisible) {
			Image imagec = gameObject.GetComponentInChildren<Image>();
			imagec.enabled = true;
			Button buttonc = gameObject.GetComponentInChildren<Button>();
			buttonc.enabled = true;

			Image image = pick.GetComponent<Image>();
			image.enabled = true;
		} else {
			Image imagec = gameObject.GetComponentInChildren<Image>();
			imagec.enabled = false;
			Button buttonc = gameObject.GetComponentInChildren<Button>();
			buttonc.enabled = false;

			Image image = pick.GetComponent<Image>();
			image.enabled = false;
		}
	}

	public void onChestClick(Button button){
		if (!data.isOpen)
			OpenChest (data);
		else
			CloseChest (data);
	}

	public void OpenChest(Model.Chest chest){
		if (!chest.isOpen) {
			chest.isOpen = true;
			level.matchGame.GetOpenChests().Add (chest);
		}
		CloseMatches ();
	}

	public void CloseMatches(){
		level.matchGame.GetOpenChests().Sort(new Sorter());
		List<Model.Chest> openChests = level.matchGame.GetOpenChests ();

		for (int i=0; i < openChests.Count - 1; i=i+2)
		{
			Model.Chest c1 = openChests [i];
			Model.Chest c2 = openChests [i+1];

			if (c1.pick.pickObject == c2.pick.pickObject) {
				StartCoroutine(HideChest (c1));
				StartCoroutine(HideChest (c2));
			} else {
				StartCoroutine(CloseChest (c1));
				StartCoroutine(CloseChest (c2));
			}
		}
	}

	//TODO: Use event Queue so we dont need the unity Engine here.
	public IEnumerator<WaitForSecondsRealtime> CloseChest(Model.Chest chest){
		yield return new WaitForSecondsRealtime(1);
		chest.isOpen = false;
		level.matchGame.GetOpenChests().Remove(chest);
	}

	//TODO: Use event Queue so we dont need the unity Engine here.
	public IEnumerator<WaitForSecondsRealtime> HideChest(Model.Chest chest){

		yield return new WaitForSecondsRealtime(1);
		chest.isVisible = false;
		chest.isOpen = false;
		level.matchGame.GetOpenChests().Remove(chest);
	}

	private void swapSprite(Sprite sprite){
		Image image = gameObject.GetComponent<Image> ();
		image.sprite = sprite;
		image.SetNativeSize ();
	}
}
